package projet;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import projet.Controller.SceneController;
import projet.View.ScoresView;

public class Main extends Application {
    private Button jouer, scores, quitter;
    private SceneController sceneController;

    @Override
    public void start(Stage primaryStage) throws Exception {

        GridPane pane = new GridPane();
        pane.getStyleClass().add("pane");

        jouer = new Button();
        scores = new Button();
        quitter = new Button();

        jouer.setPadding(Insets.EMPTY);
        jouer.setGraphic(new ImageView(new Image("projet/Images/jouer.png")));
        scores.setPadding(Insets.EMPTY);
        scores.setGraphic(new ImageView(new Image("projet/Images/scores.png")));
        quitter.setPadding(Insets.EMPTY);
        quitter.setGraphic(new ImageView(new Image("projet/Images/quitter.png")));

        jouer.setOnAction(this::handleButtonEvent);
        scores.setOnAction(this::handleButtonEvent);
        quitter.setOnAction(this::handleButtonEvent);

        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        pane.add(jouer, 0, 0);
        pane.add(scores, 0, 1);
        pane.add(quitter, 0, 2);

        primaryStage.setTitle("Candy Crush");
        Scene scene = new Scene(pane, 1600, 900);
        scene.getStylesheets().add("/projet/styles.css");

        sceneController = new SceneController(scene);
        sceneController.addScene("Main", pane);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private void handleButtonEvent(ActionEvent event) {
        Object source = event.getSource();

        if (source == jouer) {
            new customDialog(sceneController);
        } else if (source == scores) {
            sceneController.addScene("Scores", new ScoresView(sceneController));
            sceneController.activate("Scores");
        } else if (source == quitter) {
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
