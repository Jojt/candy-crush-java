package projet;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import projet.Controller.Controller;
import projet.Controller.SceneController;
import projet.Model.Plateau;
import projet.View.GameView;

class customDialog {
    private ObservableList<Integer> items = FXCollections.observableArrayList(5, 7, 10);
    private Plateau p;
    private Controller c;

    /**
     * Fenêtre popup personnalisée
     * @param sceneController Contrôleur de scène
     */
    customDialog(SceneController sceneController) {
        Dialog<gameParams> dialog = new Dialog<>();
        dialog.setTitle("Paramètres de jeu");
        dialog.setHeaderText("Paramétrer le jeu");

        ButtonType playButtonType = new ButtonType("Jouer", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(playButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Pseudo");
        ListView<Integer> tailleList = new ListView<>();
        tailleList.setItems(items);
        tailleList.setPrefSize(90, 80);
        tailleList.getSelectionModel().select(1);

        TextField score = new TextField();
        score.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}")) {
                    score.setText(oldValue);
                }
            }
        });

        TextField coups = new TextField();
        coups.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}")) {
                    coups.setText(oldValue);
                }
            }
        });

        grid.add(new Label("Nom du joueur :"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Taille du plateau :"), 0, 1);
        grid.add(tailleList, 1, 1);
        grid.add(new Label("Objectif de score :"), 0, 2);
        grid.add(score, 1, 2);
        grid.add(new Label("Nombre de coups :"), 0, 3);
        grid.add(coups, 1, 3);

        Node playButton = dialog.getDialogPane().lookupButton(playButtonType);
        playButton.setDisable(true);

        username.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!coups.getText().trim().isEmpty() || !score.getText().trim().isEmpty()) {
                playButton.setDisable(newValue.trim().isEmpty());
            }
        });

        score.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!username.getText().trim().isEmpty()) {
                playButton.setDisable(newValue.trim().isEmpty());
            }
        });

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> username.requestFocus());

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == playButtonType) {
                int index = tailleList.getSelectionModel().getSelectedIndex();

                int cleanScore = score.getText().isEmpty() ? 0 : Integer.parseInt(score.getText());
                int cleanCoups = coups.getText().isEmpty() ? 0 : Integer.parseInt(coups.getText());

                return new gameParams(username.getText(), items.get(index), cleanScore, cleanCoups);
            }
            return null;
        });

        dialog.showAndWait().ifPresent(result -> {
            p = new Plateau(result.getUsername(), result.getIndex(), result.getScore(), result.getCoups());
            c = new Controller(p);
            sceneController.addScene("Plateau", new GameView(p, c, sceneController));
            sceneController.activate("Plateau");
        });
    }
}
