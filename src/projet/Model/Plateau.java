package projet.Model;

import java.util.*;

public class Plateau extends Observable {
    private String username;
    private int score = 0;
    private int coups = 0;
    private int coupsMax;
    private int objectifScore;
    private int taille;
    private Integer[][] plateau;
    private int max = 6;
    private int min = 1;
    private Random rand = new Random();
    private boolean gameOver = false;

    public Plateau(String username, int tailleCustom, int objectifScore, int coupsMax) {
        this.username = username;
        this.taille = tailleCustom;
        this.objectifScore = objectifScore;
        this.coupsMax = coupsMax == 0 ? 999999 : coupsMax;

        plateau = createPlateau();

        setChanged();
        notifyObservers();
    }

    /**
     * Cette méthode crée la matrice d'Integer qui nous servira de plateau
     *
     * @return la matrice d'integer qui sert de plateau
     */
    private Integer[][] createPlateau() {
        Integer[][] ret = new Integer[taille][taille];

        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                int candyType = rand.nextInt(max - min + 1) + min;

                //Permet d'éviter d'avoir des bonbons alignés à la génération du plateau
                while ((j >= 2 && ret[i][j - 1] == candyType && ret[i][j - 2] == candyType) || (i >= 2 && ret[i - 1][j] == candyType && ret[i - 2][j] == candyType)) {
                    candyType = rand.nextInt(max - min + 1) + min;
                }
                ret[i][j] = candyType;
            }
        }
        return ret;
    }

    /**
     * Cette méthode vérifie si un échange est possible en appelant les autres méthodes de vérification
     *
     * @param x1 Coordonnée du 1er bonbon choisi
     * @param y1 Coordonnée du 1er bonbon choisi
     * @param x2 Coordonnée du 2d bonbon choisi
     * @param y2 Coordonnée du 2d bonbon choisi
     */
    private void echanger(int x1, int y1, int x2, int y2) {
        int tmp = plateau[x1][y1];
        plateau[x1][y1] = plateau[x2][y2];
        plateau[x2][y2] = tmp;

        Set<Coord> matches = checkBonbon(x2, y2, tmp);

        // Au moins 3 bonbons alignés
        if (matches.size() >= 3) {
            coups++;
            destructionBonbon(matches);
            descenteBonbon();
            fillBlank();
            cleanPlateau();
        } else {
            //revert
            plateau[x2][y2] = plateau[x1][y1];
            plateau[x1][y1] = tmp;
        }

        setChanged();
        notifyObservers();
    }

    /**
     * On vérifie que les 2 bonbons sont bien adjacents
     *
     * @param x1 Coordonnée du 1er bonbon choisi
     * @param y1 Coordonnée du 1er bonbon choisi
     * @param x2 Coordonnée du 2d bonbon choisi
     * @param y2 Coordonnée du 2d bonbon choisi
     */
    public void verifEchange(int x1, int y1, int x2, int y2) {
        if ((x2 == x1 + 1 && y2 == y1) || (x2 == x1 - 1 && y2 == y1) || (x2 == x1 && y2 == y1 + 1) || (x2 == x1 && y2 == y1 - 1)) {
            echanger(x1, y1, x2, y2);
        }
    }

    /**
     * Méthode principale de l'application, elle vérifie l'alignement de 3 bonbons
     *
     * @param x     Coordonné du bonbon
     * @param y     Coordonné du bonbon
     * @param value Valeur du bonbon (= type)
     * @return
     */
    private Set<Coord> checkBonbon(int x, int y, int value) {
        Set<Coord> ret = new HashSet<>();
        Set<Coord> horizontalMatches = new HashSet<>();
        Set<Coord> verticalMatches = new HashSet<>();

        Coord p = new Coord(x, y, value);

        // Permet d'également valider les bonbons spéciaux qui valent valeur du bonbon +11 ou +17 et inversement
        Set<Integer> valueSet = new HashSet<>(Arrays.asList(value, value + 11, value + 17, value - 11, value - 17));

        int i = y - 1;
        while (i >= 0 && valueSet.contains(plateau[x][i])) {
            horizontalMatches.add(new Coord(x, i, plateau[x][i]));
            i--;
        }

        i = y + 1;
        while (i < this.taille && valueSet.contains(plateau[x][i])) {
            horizontalMatches.add(new Coord(x, i, plateau[x][i]));
            i++;
        }

        i = x - 1;
        while (i >= 0 && valueSet.contains(plateau[i][y])) {
            verticalMatches.add(new Coord(i, y, plateau[i][y]));
            i--;
        }

        i = x + 1;
        while (i < this.taille && valueSet.contains(plateau[i][y])) {
            verticalMatches.add(new Coord(i, y, plateau[i][y]));
            i++;
        }

        // Si au moins 2 bonbons alignés verticalement
        if (verticalMatches.size() >= 2) {
            //On les ajoute à notre collection de "match"
            ret.addAll(verticalMatches);
            //Si exactement 2 bonbons alignés on ajoute le bonbon échangé
            if (verticalMatches.size() == 2) {
                ret.add(p);
            } else {
                //Sinon on transforme le bonbon échangé en bonbon spécial
                switch (plateau[x][y]) {
                    case 1:
                        plateau[x][y] = 12;
                        break;
                    case 2:
                        plateau[x][y] = 13;
                        break;
                    case 3:
                        plateau[x][y] = 14;
                        break;
                    case 4:
                        plateau[x][y] = 15;
                        break;
                    case 5:
                        plateau[x][y] = 16;
                        break;
                    case 6:
                        plateau[x][y] = 17;
                        break;
                    default:
                        plateau[x][y] = plateau[x][y];
                }
            }
        }

        // Si au moins 2 bonbons alignés horizontalement
        if (horizontalMatches.size() >= 2) {
            //On les ajoute à notre collection de "match"
            ret.addAll(horizontalMatches);
            //Si exactement 2 bonbons alignés on ajoute le bonbon échangé
            if (horizontalMatches.size() == 2) {
                ret.add(p);
            } else {
                //Sinon on transforme le bonbon échangé en bonbon spécial
                switch (plateau[x][y]) {
                    case 1:
                        plateau[x][y] = 18;
                        break;
                    case 2:
                        plateau[x][y] = 19;
                        break;
                    case 3:
                        plateau[x][y] = 20;
                        break;
                    case 4:
                        plateau[x][y] = 21;
                        break;
                    case 5:
                        plateau[x][y] = 22;
                        break;
                    case 6:
                        plateau[x][y] = 23;
                        break;
                    default:
                        plateau[x][y] = plateau[x][y];
                }
            }
        }

        return ret;
    }

    /**
     * Cette méthode utilise le Set de bonbons alignés pour les supprimer
     *
     * @param matches Le Set de bonbons alignés
     */
    private void destructionBonbon(Set<Coord> matches) {
        for (Coord p : matches) {
            //Si bonbon normal
            if (p.getValue() < 12) {
                plateau[p.getX()][p.getY()] = 0;
                score = score + 10;
            } else if (p.getValue() >= 12 && p.getValue() < 18) {
                //Bonbons spéciaux
                for (int i = 0; i < taille; i++) {
                    plateau[p.getX()][i] = 0;
                    score = score + 10;
                }
                plateau[p.getX()][p.getY()] = 0;
            } else if (p.getValue() >= 18 && p.getValue() < 24) {
                //Bonbons spéciaux
                for (int i = 0; i < taille; i++) {
                    plateau[i][p.getY()] = 0;
                    score = score + 10;
                }
                plateau[p.getX()][p.getY()] = 0;
            }
        }
    }

    /**
     * On parcourt la matrice depuis la case en bas à droite afin de faire descendre les bonbons
     */
    private void descenteBonbon() {
        for (int i = taille - 1; i >= 0; i--) {
            for (int j = taille - 1; j >= 0; j--) {
                int t = i;
                while (t + 1 < taille) {
                    if (plateau[t + 1][j] == 0) {
                        plateau[t + 1][j] = plateau[t][j];
                        plateau[t][j] = 0;
                    }
                    t++;
                }
            }
        }
    }

    /**
     * On remplit les "trous" du plateau
     */
    private void fillBlank() {
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                if (plateau[i][j] == 0) {
                    plateau[i][j] = rand.nextInt(max - min + 1) + min;
                }
            }
        }
    }

    /**
     * Cette méthode nettoye le plateau après chaque coup en détruisant les bonbons alignés
     */
    private void cleanPlateau() {
        //On vérifie que le joueur n'a pas gagné ou perdu
        if (score >= objectifScore || coups >= coupsMax) {
            gameOver = true;
            return;
        }

        Set<Coord> matches = checkPlateau();

        //Quand plus de bonbons alignés
        if (matches.size() == 0) {
            //Cette méthode devrait détecter les échanges possibles et déclencher un game over si plus d'échange possible
            detectMoves();
            return;
        }

        destructionBonbon(matches);
        descenteBonbon();
        fillBlank();
        cleanPlateau();
    }

    private boolean detectMoves() {
        return false;
    }

    /**
     * Appelle les 2 méthodes de parcours de la matrice pour éliminer les bonbons alignés
     *
     * @return Un set de bonbon alignés
     */
    private Set<Coord> checkPlateau() {
        Set<Coord> horizontalMatches = detectBonbonsLignes();
        Set<Coord> verticalMatches = detectBonbonsColonnes();
        Set<Coord> ret = new HashSet<>();

        ret.addAll(verticalMatches);
        ret.addAll(horizontalMatches);

        return ret;
    }

    /**
     * Détecte les bonbons alignés horizontalement
     *
     * @return Un set de bonbon alignés
     */
    private Set<Coord> detectBonbonsLignes() {
        Set<Coord> ret = new HashSet<>();

        for (int i = 0; i < taille; i++) {
            int j = 0;
            while (j < taille - 2) {
                int tmp = plateau[i][j];
                Set<Integer> tmpSet = new HashSet<>(Arrays.asList(tmp, tmp + 11, tmp + 17, tmp - 11, tmp - 17));

                if (tmpSet.contains(plateau[i][j + 1]) && tmpSet.contains(plateau[i][j + 2])) {
                    while (j < taille && plateau[i][j] == tmp) {
                        Coord p = new Coord(i, j, plateau[i][j]);
                        if (ret.contains(p)) {
                            continue;
                        } else {
                            ret.add(p);
                        }
                        j++;
                    }
                }
                j++;
            }
        }
        return ret;
    }

    /**
     * Détecte les bonbons alignés verticalement
     *
     * @return Un set de bonbon alignés
     */
    private Set<Coord> detectBonbonsColonnes() {
        Set<Coord> ret = new HashSet<>();

        for (int j = 0; j < taille; j++) {
            int i = 0;
            while (i < taille - 2) {
                int tmp = plateau[i][j];
                Set<Integer> tmpSet = new HashSet<>(Arrays.asList(tmp, tmp + 11, tmp + 17, tmp - 11, tmp - 17));

                if (tmpSet.contains(plateau[i + 1][j]) && tmpSet.contains(plateau[i + 2][j])) {
                    while (i < taille && plateau[i][j] == tmp) {
                        Coord p = new Coord(i, j, plateau[i][j]);
                        if (ret.contains(p)) {
                            continue;
                        } else {
                            ret.add(p);
                        }
                        i++;
                    }
                }
                i++;
            }
        }
        return ret;
    }


    public Integer[][] getPlateau() {
        return plateau;
    }

    public String getUsername() {
        return username;
    }

    public int getScore() {
        return score;
    }

    public int getCoups() {
        return coups;
    }

    public int getTaille() {
        return taille;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public int getObjectifScore() {
        return objectifScore;
    }
}
