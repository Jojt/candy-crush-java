package projet.Model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Représentation d'un bonbon
 */
public class Sprite extends ImageView {
    private int numero, x, y;
    private String url;

    public Sprite(int x, int y, int num) {
        this.numero = num;
        this.x = x;
        this.y = y;

        //Selon la valeur de la case de la matrice on affiche un type de bonbon
        switch (numero) {
            case 1:
                url = "projet/Images/sprites/sprite_0.png";
                break;
            case 2:
                url = "projet/Images/sprites/sprite_1.png";
                break;
            case 3:
                url = "projet/Images/sprites/sprite_2.png";
                break;
            case 4:
                url = "projet/Images/sprites/sprite_3.png";
                break;
            case 5:
                url = "projet/Images/sprites/sprite_4.png";
                break;
            case 6:
                url = "projet/Images/sprites/sprite_5.png";
                break;
            case 12:
                url = "projet/Images/sprites/sprite_12.png";
                break;
            case 13:
                url = "projet/Images/sprites/sprite_13.png";
                break;
            case 14:
                url = "projet/Images/sprites/sprite_14.png";
                break;
            case 15:
                url = "projet/Images/sprites/sprite_15.png";
                break;
            case 16:
                url = "projet/Images/sprites/sprite_16.png";
                break;
            case 17:
                url = "projet/Images/sprites/sprite_17.png";
                break;
            case 18:
                url = "projet/Images/sprites/sprite_18.png";
                break;
            case 19:
                url = "projet/Images/sprites/sprite_19.png";
                break;
            case 20:
                url = "projet/Images/sprites/sprite_20.png";
                break;
            case 21:
                url = "projet/Images/sprites/sprite_21.png";
                break;
            case 22:
                url = "projet/Images/sprites/sprite_22.png";
                break;
            case 23:
                url = "projet/Images/sprites/sprite_23.png";
                break;
            default:
                url = "projet/Images/sprites/sprite_0.png";
        }

        Image sprite = new Image(url, 75, 75, false, false);

        this.setImage(sprite);
    }

    public int getUnX() {
        return x;
    }

    public int getUnY() {
        return y;
    }

    /**
     * Effet de surbrillance quand on sélectionne un bonbon normal
     */
    public void highlight() {
        if (numero < 12) {
            int tmp = numero + 5;
            url = "projet/Images/sprites/sprite_" + tmp + ".png";
            Image sprite = new Image(url, 75, 75, false, false);
            this.setImage(sprite);
        }
    }

    /**
     * Reset de l'apparence du bonbon
     */
    public void reset() {
        if (numero < 12) {
            int tmp = numero - 1;
            url = "projet/Images/sprites/sprite_" + tmp + ".png";
            Image sprite = new Image(url, 75, 75, false, false);
            this.setImage(sprite);
        }
    }
}
