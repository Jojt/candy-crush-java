package projet.Model;

/**
 * Structure de données custom pour sauvegarder l'emplacement d'un bonbon et sa valeur (= type)
 */
class Coord {
    private int x, y, value;

    Coord(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    int getValue() {
        return value;
    }
}
