package projet.View;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import projet.Controller.SceneController;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;


public class ScoresView extends BorderPane {
    private Button retour;
    private SceneController sceneController;
    private ObservableList<String> items = FXCollections.observableArrayList ();


    public ScoresView(SceneController sceneController) {
        super();
        this.sceneController = sceneController;
        this.getStyleClass().add("pane");

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        retour = new Button();
        retour.setOnAction(this::handleButtonEvent);

        retour.setPadding(Insets.EMPTY);
        retour.setGraphic(new ImageView(new Image("projet/Images/retour.png")));

        ListView<String> list = new ListView<>();
        list.setMaxSize(250, 500);

        initScores();
        list.setItems(items);

        grid.add(new Label("Scores :"), 0, 0);
        grid.add(list, 0, 1);
        grid.add(retour, 0, 2);
        grid.setAlignment(Pos.CENTER);

        this.setCenter(grid);
    }

    private void initScores() {

        File f = new File("scores.json");
        if (f.exists()) {
            try {
                JSONParser jsonParser = new JSONParser();
                Object object = jsonParser.parse(new FileReader("scores.json"));
                JSONObject jsonObject = (JSONObject) object;

                JSONArray articles = (JSONArray) jsonObject.get("scores");
                Iterator itr = articles.iterator();

                JSONArray pseudos = (JSONArray) jsonObject.get("pseudos");
                Iterator itr2 = pseudos.iterator();

                ArrayList<String> values = new ArrayList<>();

                while (itr.hasNext() && itr2.hasNext()) {
                    values.add(itr2.next() + " : " + itr.next());
                }

                items.addAll(values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleButtonEvent(ActionEvent event) {
        Object source = event.getSource();

        if (source == retour) {
            sceneController.activate("Main");
        }
    }
}
