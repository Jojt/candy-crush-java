package projet.View;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import projet.Controller.Controller;
import projet.Controller.SceneController;
import projet.Model.Plateau;
import projet.Model.Sprite;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;

public class GameView extends BorderPane implements Observer {
    private Plateau p;
    private Controller c;
    private Sprite[][] matriceSprites;
    private GridPane gridPane;
    private boolean clic = false;
    private Sprite origineClic;
    private Button retour;
    private SceneController sceneController;
    private Label l4, l6, l8;

    public GameView(Plateau p, Controller c, SceneController sceneController) {
        super();
        this.p = p;
        this.c = c;
        this.sceneController = sceneController;
        this.p.addObserver(this);
        this.getStyleClass().add("pane");

        GridPane leftGridPane = new GridPane();

        Label l1 = new Label("Joueur : ");
        Label l2 = new Label(c.getUsername());
        Label l3 = new Label("Score : ");
        l4 = new Label("0");
        Label l5 = new Label("Nombre de coups : ");
        l6 = new Label("0");
        Label l7 = new Label("Objectif de score : ");
        l8 = new Label(Integer.toString(c.getObjectifScore()));


        retour = new Button();
        retour.setOnAction(this::handleButtonEvent);
        retour.setPadding(Insets.EMPTY);
        retour.setGraphic(new ImageView(new Image("projet/Images/retour.png")));

        leftGridPane.add(l1, 0, 0);
        leftGridPane.add(l2, 1, 0);
        leftGridPane.add(l3, 0, 1);
        leftGridPane.add(l4, 1, 1);
        leftGridPane.add(l5, 0, 2);
        leftGridPane.add(l6, 1, 2);
        leftGridPane.add(l7, 0, 3);
        leftGridPane.add(l8, 1, 3);
        leftGridPane.add(retour, 0, 4);

        gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        initMatriceSprites();
        this.setCenter(gridPane);
        this.setLeft(leftGridPane);
    }

    /**
     * Cette fonction parcourt la matrice et y superpose une matrice de Sprite affichant les bonbons
     */
    private void initMatriceSprites() {
        int taille = c.getTaille();
        matriceSprites = new Sprite[taille][taille];

        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {

                int value = c.getPlateau()[i][j];

                Sprite sprite = new Sprite(i, j, value);
                matriceSprites[i][j] = sprite;
                sprite.setOnMouseClicked(spriteListener(sprite));

                gridPane.add(matriceSprites[i][j], j, i);
            }
        }
    }

    /**
     * Chaque bonbon à un listener permettant de connaître le bonbon cliqué
     * @param sprite Le bonbon cliqué
     * @return
     */
    private EventHandler<? super MouseEvent> spriteListener(Sprite sprite) {
        return event -> {
            try {
                if (!clic) {
                    clic = true;
                    premierClic(sprite);
                } else {
                    clic = false;
                    secondClic(sprite);
                }
            } catch (Exception e) {
                System.out.println("Déplacement non autorisé");
            }
        };
    }

    /**
     * Au premier clic on met en surbrillance le bonbon sélectionné et on le sauvegarde en variable de classe
     * @param sprite Le bonbon sélectionné
     */
    private void premierClic(Sprite sprite) {
        origineClic = sprite;
        origineClic.highlight();
    }

    /**
     * Au deuxième clic on lance une vérification de l'échange avec les coordonnées des 2 bonbons
     * @param sprite Le deuxième bonbon sélectionné
     */
    private void secondClic(Sprite sprite) {
        c.echanger(origineClic.getUnX(), origineClic.getUnY(), sprite.getUnX(), sprite.getUnY());
        origineClic.reset();
    }

    private void handleButtonEvent(ActionEvent event) {
        Object source = event.getSource();

        if (source == retour) {
            saveData();
        }
    }

    /**
     * Fonction de sauvegarde des scores
     */
    private void saveData() {
        File f = new File("scores.json");
        if (!f.exists()) {
            try {
                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArrayScore = new JSONArray();
                jsonArrayScore.add(c.getScore());

                JSONArray jsonArrayUser = new JSONArray();
                jsonArrayUser.add(c.getUsername());

                jsonObject.put("scores", jsonArrayScore);
                jsonObject.put("pseudos", jsonArrayUser);

                FileWriter fileWriter = new FileWriter("scores.json");
                fileWriter.write(jsonObject.toJSONString());
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                JSONParser jsonParser = new JSONParser();
                Object object = jsonParser.parse(new FileReader("scores.json"));
                JSONObject jsonObject = (JSONObject) object;

                JSONArray jsonArrayScore = (JSONArray) jsonObject.get("scores");
                jsonArrayScore.add(c.getScore());

                JSONArray jsonArrayUser = (JSONArray) jsonObject.get("pseudos");
                jsonArrayUser.add(c.getUsername());

                jsonObject.put("scores", jsonArrayScore);
                jsonObject.put("pseudos", jsonArrayUser);

                FileWriter fileWriter = new FileWriter("scores.json");
                fileWriter.write(jsonObject.toJSONString());
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sceneController.activate("Main");
    }

    /**
     * à chaque update on vérifie si le jeu n'est pas fini, sinon on recrée notre matrice de sprite
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        if (c.isGameOver()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Fin de la partie");
            alert.setHeaderText("Game Over");

            if (c.getScore() < c.getObjectifScore()) {
                alert.setContentText("Perdu ! Votre score : " + c.getScore() + ", votre objectif de score : " + c.getObjectifScore());
            } else {
                alert.setContentText("Gagné ! Votre score : " + c.getScore() + ", votre objectif de score : " + c.getObjectifScore());
            }

            ButtonType buttonTypeOne = new ButtonType("Retour");

            alert.getButtonTypes().setAll(buttonTypeOne);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                saveData();
            }
        } else {
            //On enlève tous les sprites
            gridPane.getChildren().clear();

            //Mise à jour des scores et coups joués
            l4.setText(Integer.toString(c.getScore()));
            l6.setText(Integer.toString(c.getCoups()));

            //On recrée notre matrice de sprites
            initMatriceSprites();
        }
    }
}
