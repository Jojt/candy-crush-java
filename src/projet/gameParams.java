package projet;

class gameParams {
    private String username;
    private int index, score, coups;

    gameParams(String username, int index, int score, int coups) {
        this.username = username;
        this.score = score;
        this.coups = coups;
        this.index = index;
    }

    String getUsername() {
        return username;
    }

    int getScore() {
        return score;
    }

    int getCoups() {
        return coups;
    }

    int getIndex() {
        return index;
    }

}
