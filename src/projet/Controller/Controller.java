package projet.Controller;

import projet.Model.Plateau;

public class Controller {
    private Plateau p;

    public Controller(Plateau p) {
        this.p = p;
    }

    public void echanger(int x1, int y1, int x2, int y2) {
        p.verifEchange(x1, y1, x2, y2);
    }

    public Integer[][] getPlateau() {
        return p.getPlateau();
    }

    public int getTaille() {
        return p.getTaille();
    }

    public int getScore() {
        return p.getScore();
    }

    public int getCoups() {
        return p.getCoups();
    }

    public int getObjectifScore() {
        return p.getObjectifScore();
    }

    public String getUsername() {
        return p.getUsername();
    }

    public boolean isGameOver() {
        return p.isGameOver();
    }
}