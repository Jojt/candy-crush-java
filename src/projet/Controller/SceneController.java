package projet.Controller;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;

import java.util.HashMap;

/**
 * Contrôleur des scènes de l'application, permet de facilement passer d'une scène à une autre
 */
public class SceneController {
    private HashMap<String, Pane> sceneMap = new HashMap<>();
    private Scene main;

    public SceneController(Scene main) {
        this.main = main;
    }

    public void addScene(String name, Pane pane) {
        sceneMap.put(name, pane);
    }

    public void removeScene(String name) {
        sceneMap.remove(name);
    }

    public void activate(String name) {
        main.setRoot(sceneMap.get(name));
    }
}